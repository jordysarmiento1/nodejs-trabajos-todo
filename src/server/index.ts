import express from "express";
import cors from "cors";
import routes from '../routes/index.routes';
import DB_CONNECTION from '../database/index';
import http from 'http';
import { Server as server, Socket } from 'socket.io';
import jwt from 'jsonwebtoken';
import { getDeliverySocket, updateDeliveryState } from "../shared/socket/delivery.socket";

export let io: server;

class Server {
    app = express();
    server = http.createServer(this.app);

    constructor() {
        this.DB_CONNECTION();
        this.middlewares();
        this.routes();
    }

    async DB_CONNECTION() {
        await DB_CONNECTION();
    }

    middlewares() {
        this.app.use(cors());
        this.app.use(express.json());
    }

    routes() {
        this.app.use("/api", routes);
    }

    listen() {
        const PORT = process.env.PORT;
        const url: string = `http://localhost:${PORT}`;
        this.server.listen(PORT, () => {
            console.log(`El servidor se esta ejecutando en el puerto ${PORT}, visite: ${url} `);
            console.log(`El ambiente de desarrollo es: ${process.env.NODE_ENV}`)
        });
    }

    socket() {
        io = new server(this.server, {
            cors: {
                origin: '*',
                methods: ["GET", "POST", "PUT", "PATCH", "DELETE"]
            }
        });

        io.on('connection', (client: Socket) => {
            client.on('testing', data => {
                console.log("Se ejecuto testing: ", data);
            })

            client.on('join-to-my-room', (token) => {
                const { uid }: any = jwt.decode(token);
                client.join(uid);
                console.log('client added to room ', uid)
            })
            /*
                client.on('join-toMyRooms', async (token) => {
            
                    const user = await UserModel.findById(uid);
            
                    if(user.role === '1'){
                        client.join('sede A')
                    }
                    if(user.role === '2'){
                        client.join('sede B')
                    }
            
                })
            */
            client.on('delivery-tracking', async (token: string) => {
                const { uid }: any = jwt.decode(token);
                const data = await getDeliverySocket(uid);
                console.log({ data })
                io.to(client.id).emit('delivery-getted', data);
            })

            client.on('delivery-updated', async ({ deliveryId, newState, userId }) => {
                const data = await updateDeliveryState(deliveryId, newState);
                console.log("delivery updated")
                io.to(userId).emit('delivery-update', data)
            })

            console.log('client connected!')
        })
    }
}

export default Server;