import Server from './server';
import { compare64, decrypt64, encrypt64 } from './shared/utils/crypt';
require('dotenv').config();

const server = new Server();

server.listen();
server.socket();

const name = 'jordysarmiento';
const encryptname = encrypt64(name);
console.log(encryptname);
const decryptname = decrypt64(encryptname);
console.log(decryptname);
const compare = compare64(encryptname, name);
console.log(compare)
