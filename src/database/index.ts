import mongoose from 'mongoose';

const DB_CONNECTION = async () => {
	try {
		await mongoose.connect(process.env.DB_CONNECTION!.replace("<user>", process.env.DB_USER!).replace("<password>", process.env.DB_PASSWORD!))
		console.log("La base de datos se ha conectado satisfactoriamente!");
	} catch (error) {
		Promise.reject(error);
	}
}

export default DB_CONNECTION;